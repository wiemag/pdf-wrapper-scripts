#!/bin/bash
# Imagemagick's (convert) wrapper.
# Converts PDF's into monochrome PDF's.
# qpdf - optional dependecy
VERSION="2.02" # change the depricated 'convert' command to magick.
tty -s && TERM=1 || TERM=0 	# Script run from a terminal

function usage () {
	echo -e "\nUsage:\n\t\e[1m${0##*/} [-options] FileName[.pdf]\e[0m\n"
	echo -e "Produces a monochrome PDF named:  \e[33;1mFileName_mono.pdf\e[0m\n"
	cat <<OPCJE
Options:

-d Density
      Ensures the quality of resulting image.
      Default density: 288. Sane values [144..400]."
-c CompressionType
      Use the types as defined for imagemagick's convert tool:
      Group4, Fax (default), JPEG, JPEG2000, Lossless
      LZW, RLE, ZIP, BZip, or just None.
      Command
         magick -list compress
      will show a full list of compression types.
-t Threshold
      Could be used to remove light grey spots.
      By default, the option is NOT used.
      Values (0%..100%). Try 80% or 90%.
-i
      In-place conversion
-r Size
      Resize the output according to the Size(%) parameter.
-q    Quiet/Silent run. No echo printed.
-m
      Limit memory to 6GiB and
      set directory ~/.tmp for temporary files.
-v    makes the output verbose.
OPCJE
echo -e "\n'\e[1m-monochrome\e[0m' option is hardcoded into the conversion command.\n"
}

declare -i DENS 	# Density is supposed to be integer.
VERB="" 		# Verbose? No. (default)
THRE="72%" 		# Threshold (whiten out or enhance), a default value of 72%
SIZE="" 		# Resize (0%..100%]
INPLACE=0		# Do the conversion in place, no backup.
QUIET=0			# Default = print messages
MEM="" 		# Will memory be limited, and temp-files directory created?
TMPD="${HOME}/.tmp" # Temporary path set for big files


while getopts "vd:c:t:ir:qmhV" flag
do
 case "$flag" in
  v) VERB="-verbose";;
  d) DENS="$OPTARG";;
  c) CMPR="$OPTARG";;
  t) THRE="$OPTARG";;
  i) INPLACE=1;;
  r) SIZE="-resize $OPTARG";;
  m) MEM="-monitor -define registry:temporary-path=$TMPD -limit memory 6GiB -limit map 6GiB"; [[ -e $TMPD ]] || { mkdir -p $TMPD;} ;;
  q) QUIET=1;;
  h) usage; exit;;
  V) echo -e "${0##*/} version ${VERSION}" && exit;;
 esac
done
# Remove the options parsed above.
shift `expr $OPTIND - 1`
(( $# )) || { usage; exit;}

# If the script is not run from a Terminal, set the threshold to 78%.
DENS=${DENS:-288} 		# the default density of 288
((DENS>0)) || DENS=288	# if not positive, the default one assumed
CMPR=${CMPR:-Group4} 	# the default compression of Group4
CMPRList=$(echo $(magick -list compress))
[[ $CMPRList =~ (^| )$CMPR($| ) ]] || { echo Wrong compression type.; exit 1;}
CMPR='-compress '$CMPR
((TERM)) && {
	((QUIET)) || {
		echo density=${DENS};
		echo compress=${CMPR};
	}
} || notify-send "${0##*/} ${1}" "Options: -threshold ${THRE} -density ${DENS} ${CMPR}" --icon=dialog-information;
TempPDF=$(mktemp)
TempPS=$(mktemp)
while [[ $# -gt 0 ]]; do
 f=${1}
 [[ -f "$f" ]] || f=${f}.pdf 	# As the pdf extension is optional.
 if [[ -f "$f" ]]; then
   nf=$f
   ((INPLACE)) || {
	ext=${f##*.};
	[[ ${ext,,} = 'pdf' ]] && nf=${nf%.*} #${ext,,} lowercase
	nf="${nf}_mono.pdf"
   }
   bn=${f##*/}
   ((TERM)) && {
	if [[ $QUIET -eq 0 ]]; then	
	  echo DEBUG: \$nb=${bn};
	  echo magick $MEM $VERB -density $DENS "$f" $SIZE -threshold $THRE -monochrome $CMPR "$nf";
	fi
   }
   magick $MEM $VERB -density $DENS "$f" $SIZE -threshold $THRE $CMPR -monochrome "$TempPDF"

   # If error in resultant file, repair it by repackaging it with pdftops and ps2pdf.
   gs -dBATCH -dNOPAUSE -sDEVICE=nullpage "$TempPDF" |grep 'Error\|error' && {
	pdftops "$TempPDF" "$TempPS";
	ps2pdf "$TempPS" "$TempPDF";
	((TERM)) && echo PDF has been repackaged with pdftops and ps2pdf.
   }
   # If qpdf installed, remove meta data.
   hash qpdf 2>/dev/null && {
	((INPLACE)) || nf="${nf%.pdf}_meta.pdf"
	qpdf -empty -pages "$TempPDF" 1-z -- "$nf";
   } || mv "$TempPDF" "$nf"
   chmod $(stat -L -c "%a" "$f") "${nf}" # set permitions
  else
   ((TERM)) && {
	[[ "$f" =~ '*' ]] && echo Files $f do not exist. || 
		{ ((QUIET)) || echo File $f does not exist.;}
   }
 fi
 shift
done;
rm "$TempPS"
[[ -f "$TempPDF" ]] && rm "$TempPDF"
