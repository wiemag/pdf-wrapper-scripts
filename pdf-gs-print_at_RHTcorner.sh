#!/bin/bash
VERSION="version 0.02"
info_msg(){
cat <<INFO

  ${0##*/} 't e x t' input.pdf output.pdf font_size

• Print text at the right-hand top corner of the input.pdf.
• Parameters:
	text_to_print
	input.pdf
	[output.pdf]  (defaults to "\${output%.pdf}_RH_text.pdf")
	[font_size]   (defaults to 16)
	[font_colour] (defaults to '1 1 1')
	Extra colour names are defined:
	   black = '0 0 0'        gray|grey = '0.7 0.7. 0.7'
	   red    = '1 0 0'       green   = '0 1 0'           blue   = '0 0 1'
	   aqua|cyan  = '0 1 1'   fuchsia|magenta = '1 0 1'   yellow = '1 1 0'
	   blue  = '0.5 0.5 1'    teal  = '0 0.4 0.4'         navy   = '0 0 0.5'
	   white = '1 1 1'
• Hard-coded parameters:
	font_face (Helvetica)
• No checking if parameters are correct.
• Text in colour white to hide the text on a white-background page.
INFO
}

[[ "${1-}" == '-h' || "${1-}" == '--help' ]] && { info_msg; exit 0;}
(($# < 3)) &&  { echo; echo Missing input; info_msg; exit 1;}

# Parameters
TEXT_TO_PRINT="$1"
input="$2"
output="${3:-${input%.pdf}_RH_text.pdf}"
fntsize="${4:-16}"
fntcolr="${5:-1 1 1}"
fntcolr="${fntcolr,,}"
if [[ "$fntcolr" != '1 1 1' ]]
then
	if [[ "$fntcolr" == 'black' ]]
	then
		fntcolr='0 0 0'
	elif [[ "$fntcolr" == 'grey' || "$fntcolr" == 'gray' ]]
	then
		fntcolr='0.7 0.7 0.7'
	elif [[ "$fntcolr" == 'red' ]]
	then
		fntcolr='1 0 0'
	elif [[ "$fntcolr" == 'green' ]]
	then
		fntcolr='0 1 0'
	elif [[ "$fntcolr" == 'blue' ]]
	then
		#fntcolr='0.5 0.5 1'
		fntcolr='0 0 1'
	elif [[ "$fntcolr" == 'yellow' ]]
	then
		fntcolr='1 1 0'
	elif [[ "$fntcolr" == 'aqua' || "$fntcolr" == 'cyan' ]]
	then
		fntcolr='0 1 1'
	elif [[ "$fntcolr" == 'fuchsia' || "$fntcolr" == 'magenta' ]]
	then
		fntcolr='1 0 1'
	elif [[ "$fntcolr" == 'teal' ]]
	then
		fntcolr='0 0.4 0.4'
	elif [[ "$fntcolr" == 'navy' ]]
	then
		fntcolr='0 0 0.5'
	elif [[ "$fntcolr" == 'white' ]]
	then
		fntcolr='1 1 1'
	fi
fi


# Ghostscript code that prints the text
gs_print_code="
%!PS
%
% gs -q -o output.pdf -dNOPAUSE -dSAFER -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=a4 ps_code.ps input.pdf

% Definitions of/Checking page size
/pageWidth { currentpagedevice /PageSize get 0 get } def
/pageHeight { currentpagedevice /PageSize get 1 get } def
%pageWidth		% put pageWidth value on stack
%pageHeight		% put pageHeight value on stack
%= =			% print pageHeight and pageWidth on screen, removing from stack
%/vpos pageHeight def		% set initial vertical position
%/hpos 100 def				% set initial vertical position
%/setBlueText { 1 1 1 setrgbcolor } def  % define text color blue

/cornerFont { /Helvetica findfont ${fntsize} scalefont setfont} def
/cornerText {(${TEXT_TO_PRINT})} def
%/cornerColor { .75 setgray } def  % 1=white, 0=black
/cornerColor { ${fntcolr} setrgbcolor } def  % define text color blue

<< /EndPage
{ 0 eq
  { 0 eq
    {
      pop
      cornerFont
      newpath
      0 0 moveto
      cornerText 	% put text on stack
      dup			% duplicate the text, it will be consumed by the next line
      true charpath pathbbox
      3 -1 roll sub neg 3 1 roll sub exch	% print from start point
      %1 index 		% duplicate penultimate element
      %= 			% print the value on screen and take it off the stack
      %dup =		% duplicate the last element, print on screen & consume
      %stack		% print what's on stack
      pageWidth pageHeight moveto 	% go to top-right corner
	  rmoveto 		% relative move to printing point
      cornerColor	% set text color
      show 			% show the text
    } if
    true
  }
  {
  	pop false
  }
  ifelse
} >> setpagedevice"

# Actual command that produces the output PDF.
gs -q -o "$output" -dNOPAUSE -dSAFER -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=a4 <(cat <<<"$gs_print_code") "$input"
