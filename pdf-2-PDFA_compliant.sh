#!/usr/bin/bash
# https://ghostscript.com/docs/9.54.0/VectorDevices.htm#PDFA
version=0.1  # June 2024
(($#)) || { echo -e "Missing input."; exit 1;}

usage() {
cat <<NFO
Run:
	${0##*/} [--optimised | -o] PDF | --help|-h

--optimied | -o  :  Make the document web-optimised / linearised.
NFO
exit;
}
[[ "$1" == '--help' || "$1" == '-h' ]] && usage

[[ "$1" == '--optimised' || "$1" == '-o' ]] && { OPTIMISE='true'; shift;} || OPTIMISE='false'

(($#)) || { echo Missing input.; exit 2;}
input="$1"
output="${input%.PDF}"
output="${output%.pdf}.pdf-A.pdf"

gs \
	-dPDFA \
	-dPDFACompatibilityPolicy=1 \
	-dBATCH -dNOPAUSE -dSAFER \
	-dCompressFonts=true \
	-dEmbedAllFonts=true \
	-dDetectDuplicateImages=true \
	-dFastWebView="$OPTIMISE" \
	-sColorConversionStrategy=UseDeviceIndependentColor \
	-sProcessColorModel=DeviceRGB \
	-sDEVICE=pdfwrite \
	-sOutputFile="$output" "$input"

[[ "$?" -eq 0 ]] && echo "$output" && \
sed -n '/PDF\|Optimized/p' <(pdfinfo "$output")
