#!/bin/bash
VERSION=0.98
password='' # one password for all input files
inplace=0   # replace input file

# Add bew features? -d for output directory?
# Extra: --is-encypted, --show-encryption

hlpmsg(){
	cat <<OPTS

Run:
  ${0##*/} [-i] [-p password] input1 [input2 [input3]...]
or
  ${0##*/} -h | --help | -v

This 'qpdf' wrapper decrypts input PDF's into input_decr PDF's,
the same flags/passwords for all input files.

-p  -- if not invoked, user will be prompted for password
-i  -- inplace decription; The original replaced with a decrypted file.
-h  -- print help message
-v  -- print script-version number and leave
OPTS
	exit
}

[[ "$1" == '--help' ]] && hlpmsg

while getopts "ip:hv" flag; do
    case "$flag" in
        i) inplace=1;;
        p) password="$OPTARG";;
        h) hlpmsg;;
        v) echo -e "\n${0##*/} by wiemag, version ${VERSION}" && exit;;
    esac
done
shift $((OPTIND - 1))
(($#)) || hlpmsg

[[ -z "$password" ]] && read -r -p "Enter the password: " -s password

for input in "$@"; do
	((inplace)) &&
	qpdf --decrypt --password="$password" --replace-input "${input}" ||
	qpdf --decrypt --password="$password" "${input}" "${input%.pdf}_decr.pdf"
done
