#!/usr/bin/bash
version=0.01 # July 2024

# The script uses Ghostscript (gs)
#  -dFILTERIMAGE:  produces pdfs with raster images removed
#  -dFILTERTEXT:   produces pdfs with text elements removed
#  -dFILTERVECTOR: produces pdfs with vector drawings removed

orig="${1:-}"
[[ -z "$orig" || "$orig" == '-h' || "$orig" == '--help' ]] && {
	echo -e "\nRun:\n\t$0 input.pdf\n";
	echo The script removes all text elements from the input PDF;
	echo -e "and creates \${input%.pdf}_noText.pdf\n";
	echo -e "Dependency:  ghostscript (gs)\n"
	echo -e "${0##*/}, version ${version}\n"
	exit;
}

hash qpdf 2>/dev/null && qpdf --show-pages --with-images "$orig"

gs -o "${orig%.pdf}_noText.pdf" -sDEVICE=pdfwrite -dFILTERTEXT "$orig"
