#!/bin/bash
# Dependencies:  qpdf
#
# https://github.com/qpdf/qpdf/issues/132
#
VERSION="1.00" 		# 2023-12-22
debug=0

function usage() {
echo -ne "\nUsage:\n\n\e[1m  ${0##*/} [-i] [-p page_range] [-r direction]"
echo -e " FileName[.pdf] | -h | -v\e[0m\n"
cat << INFO
This is a 'qpdf' rotation function wrapper.
It rotates page_range pages acc. to direction (right/+90, left/-90, down/+180).'
And produces an output file named:  FileName_rot.pdf"

 -i              :  An inplace rotation, modifying the input file.
 -p  page_range  :  Defaults to 1-z, i.e. all the pages.

Allowed page_range formats:
   - page_number(s), e.g 7 or 3,1 (coma separated)
   - page_range, e.g 3-6 or 4-z, or 1-9:even, or 4-7:odd
   - combined, eg 1,3,5-7,9,11-z
Note:  If a page number is repeated, the page is rotated again.

 -r direction    :  Defaults to rotate 'right' (+90g)
   Option rotate (relative adjustments):
   - left / -90,  right / +90,  down / +180
   Option set position:
   - north /  0,  east / 90,  south / 180,  west / 270
Note:  It is enough to use the first letter of the direction.

 -h              :  Prints this help message.
 -H              :  General HELP message for 'qpfg --rotation...'
 -v              :  Prints the script version number.

INFO
}

function qpdf_HELP_msg() {
cat << INFO

General qpdf rotation help message:

  qpdf in.pdf out.pdf --rotate=[+|-]angle:page-range
  qpdf in.pdf out.pdf --rotate=90:1-z:even
  qpdf in.pdf out.pdf --rotate=90:1-z:odd
  qpdf in.pdf out.pdf --rotate=0[:page-range]
e.g.
  qpdf in.pdf out.pdf --rotate=+90:2,4,6 --rotate=180:7-9

The --rotate= option may be repeated.
The angle may be 90, 180, 270, or 0.
Using +/- makes rotation relative to the current page position.
Dropping +/- (re-)sets the position of the pages.
The 0 angle resets the page to its original/drawn position.
The +0 angle does nothing.

INFO
}

function rot_direction() {
local d="${1}"
((debug)) && echo "DEBUG: Initial angle = ${d}" >&2
if [[ $d =~ ^[+-]*[0-9]+$ ]]; then
	local sign="${d:0:1}"
	[[ "$sign" =~ ^[+-]$ ]] || sign=''
	d=$(($d % 360)) # This removes '+'. The sign must be determined before.
	d="${d#-}" # No need to remove the '+' this way. See the comment above.
	((debug)) && echo "DEBUG: Angle = ${sign}${d}" >&2
	case "$d" in
		0|90|180|270) echo "${sign}${d}";;
		*) echo Wrong rotation angle... aborting. >&2;;
	esac
else
	d="${d:0:1}"
	case "${d,,}" in
		r) echo '+90';;  # right,  +90 degrees
		l) echo '-90';;  # left,   -90 degrees
		d) echo '+180';; # down,  +180 degrees
		e) echo 90;;     # east,    90 degrees
		w) echo 270;;    # west,   270 degrees
		s) echo 180;;    # south,  180 degrees
		n) echo 0;;      # north,    0 degrees
		*) echo Wrong rotation direction... aborting. >&2;;
	esac
fi
}

PAGES='1-z'
ANGLE='+90'
ROTOPTS=''
INPLACE=0
while getopts "p:r:hHvid" flag
do
    case "$flag" in
		p) PAGES="$OPTARG";;
		r) ANGLE="$(rot_direction "$OPTARG")"; [[ -z $ANGLE ]] && exit 16;;
		h) usage; exit;;
		H) qpdf_HELP_msg; exit;; # A general qpdf rotation help message.
		v) echo -e "${0##*/} version ${VERSION}" && exit;;
		i) INPLACE=1;;
		d) debug=1;;
	esac
done

# Remove the options parsed above.
shift $(($OPTIND - 1))
(($#)) || { usage; echo -e "\n\e[31;1mMissing input PDF.\e[0m" ; exit 1;}

for f in "$@"; do
	[[ -f "$f" ]] || f="${f}.pdf" # As the pdf extension is optional.

	if [[ -f "$f" ]]; then
		nf="$f"
		ext="${f##*.}"; [[ "${ext,,}" == 'pdf' ]] && nf="${nf%.*}"
		nf="${nf}_rot.pdf"
		((debug)) && echo qpdf \"$f\" \"$nf\" --rotate=\"${ANGLE}:${PAGES}\" >&2
		qpdf "$f" "$nf" --rotate="${ANGLE}:${PAGES}"
		[[ $INPLACE -eq 1 ]] && mv "$nf" "$f" || :
	else
		usage
		echo -e "\n\e[31;1mFile $f does not exist.\e[0m"
	fi
done
