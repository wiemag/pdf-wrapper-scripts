#!/bin/bash
# Imagemagick's convert wrapper.
# Converts PDF's into monochrome PDF's.
# libnotify - dependency (notify if run in graphical mode)
# ghostscript - optional dep (look for errors)
# qpdf - optional dependecy (remove meta data)
version='2.00'
verDate='2024-06-01'
source bashf.sh || {
	echo Install bashf or put bashf.sh in the \$PATH.
	exit 1
}
show_version() { echo ${0##*/}, version ${version}, $verDate; exit; }

function percent() {
	# Ensure that the value is '%' -- converts integers into per cent numbers
	local p="$1"
	is_integer "${p%\%*}" && echo "${p%\%*}%" || echo "$p";
}

TempPDF=$(mktemp)
function cleanup() { rm $TempPDF;}
trap cleanup HUP INT TERM

function is_cmd_available() {
  [[ $# -gt 1 ]] || { hash "$1" 2>/dev/null; return $?;}
  hash "$1" 2>/dev/null && { echo "$2"; return 0;} || { echo "$3"; return 1;}
}

# VARIABLES
density=288 	# Density is supposed to be integer.
IM_verbose="" 	# Imagemagick verbose mode? (default: no)
threshold="" 	# Threshold (whiten out or enhance)
resize="" 		# Resize (0%..100%]
memory="" 		# Will memory be limited, and temp-files directory created?
tmpDir="${HOME}/.tmp" # Temporary path set for big files

ARG_PARSE_OPTS+=(
	t threshold --desc='The threshold [0..100%] whitens/blackens pixels' =:val
	c compress --desc='Compression type as defined in "magick -list compress"' =:val
    d density --desc='Pixel density (default: 288 px/in)' =:val
	r resize --desc='Resize (0..100%] before conversion' =:val
#	td tmpDir --desc="Temporary directory if 'm' flag used" =:val
	m memory --desc="RAM size limit in GiB;  Do convertion in '$tmpDir'" =:val
    V IM_verbose --desc='Imagemagick verbose conversion mode' =1
	v version --desc='Print script version no and exit' '{ show_version; }'
)
ARG_PARSE_REST=(--opt-var=files --opt-min=0)
arg_parse "$@"
has_val files || die_return 2 "No input files" # or opt-min=1

im_args1=()
[ -z "$memory" ] || im_args1+=(-monitor -define registry:temporary-path=$tmpDir -limit memory ${memory}GiB -limit map ${memory}GiB)
[ -n "$memory" -a ! -d "$tmpDir" ] && { mkdir -p $tmpDir;}
[ -z "$IM_verbose" ] || im_args1+=(-verbose)
[ -z "$resize" ] || im_args1+=(-resize "$(percent $resize)")

non_strict
((density)) || {
	log_warn "Wrong density ($density). The default one (288 px/in) assigned instead.";
	density=288;
}
strict
im_args1+=(-density $density)

im_args2=()

tty -s || {
	threshold="72%";
	notify-send "${0##*/} ${1}" "Options: -threshold ${threshold} -density ${density} ${compress}" --icon=dialog-information;
}
[ -z "$threshold" ] && log_info "Parameter -threshold not used." || im_args2+=(-threshold "$(percent $threshold)")
im_args2+=(-monochrome)
compress=${compress:-Group4} 	# the default compression: Group4
grep -q $compress <(magick -list compress) || die_return 3 "Wrong compression type"
im_args2+=(-compress $compress)

# All (removing meta data, etc.) conversion moved to RAM
for f in "${files[@]}"
do
	[ -f "$f" ] || die_return 4 "Fila '$f' does not exist"
	[ "$(file -b "$f"|cut -d" " -f1)" = 'PDF' ] || die_return 5 "File '$f' is not a PDF"
	of=$f
	ext=${f##*.}		# file extension
	[[ ${ext,,} = 'pdf' ]] && of=${of%.*}
	of=${of}_mono$(is_cmd_available qpdf _meta '').pdf	# output file name
	log_info magick "${im_args1[@]}" "$f" "${im_args2[@]}" "$of"
	magick "${im_args1[@]}" "$f" "${im_args2[@]}" "$TempPDF"
	# Check it if there is an error in the output
	is_cmd_available gs && {
		gs -dBATCH -dNOPAUSE -sDEVICE=nullpage "$TempPDF" |grep -q 'Error\|error' && log_info "Ghostscript reports an error in the output file."
	}
	# If qpdf installed, remove meta data.
	is_cmd_available qpdf && {
		qpdf -empty -pages "$TempPDF" 1-z -- "$of"
		rm "$TempPDF"
	} || {
		mv "$TempPDF" "$of"
	}
done
