#!/usr/bin/bash
version=0.03 # replacing char_subset with grep -o .|sort -u|tr -d '\n'

usage_msg() {
echo -e "Run:\n\t\e[1m${0##*/} [font_name [font_size]] [alphabet]\e[0m\nor"
echo -e "\tstring from stdin | \e[1m${0##*/} [font_name [font_size]]\e[0m\n"
cat <<INFO
Create a PDF page with the alphabet letters (including space) witten
with the font_name (defalt: Arial) of font_size (default: 12).

If the alphabet is pumped through a pipe or passed as an argument,
the script uses such alphabet's characters to generate the PDF.
Otherwise, a full alphabet is used, and that includes most of the characters
from the space to the 00FF unicode value.
INFO
echo -en "\n\e[1m${0##*/} -a | --alphabet | --alfabet\e[0m"
echo "     shows the default alfabet."
echo -en "\e[1mfc-list : family -q 'font_name'\e[0m"
echo "  lists all available fonts/font families."
echo -en "\e[1mfc-list -q 'font_name'\e[0m"
echo "             answers whether the font is available."
echo -e "\e[1mLANG=C fc-list -f '%{family}, %{style}\\\n'|cut -d, -f1,2|sort -u\e[0m"
}

version_msg(){
	echo ${0##*/}, v.$version
}

alfabet() {
local a=' !"#$%'   	# includes doublequote (quotedbl)
	a+="&'()*+,-./0123456789:;<=>?@\n"	# includes singlequote (quotesingle)
	a+="$(printf '%s' {A..Z})[\]^_\`\n"
	a+="$(printf '%s' {a..z}){|}~\n"
	a+="¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿"
	a+="ÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"
#	a+="\n−ĄĆĘŁŃŚŻŹąćęłńśżź" # codes over 00FF
	echo -e "$a"
}

ALBET='' # holds a string (the full alfabet or a sub-set thereof)
FONT="${1:-Arial}"
FONTSIZE="12"
TMP_HTM="$(mktemp --suffix=.html)"  # .html

IN="${1:-}"
[[ "$IN" == '--help' || "$IN" == '-h' ]] && { usage_msg; exit;}
[[ "$IN" == '--version' || "$IN" == '-v' ]] && { version_msg; exit;}
[[ "$IN" == '-a' || "$IN" == '--alfabet' || "$IN" == '--alphabet' ]] && {
	alfabet;
	exit;
}

# The piped alphabet has a priority over the alphabet passed as an argument.

if [[ -p /dev/stdin ]]
then
	hash char_subset.sh && ALBET="$(cat | char_subset.sh)" || \
		ALBET="$(cat |grep -o .|sort -u|tr -d '\n')"
	#echo "$ALBET" >&2
	#echo "$ALBET" |wc -c >&2
fi

if [[ -n "$IN" ]]
then
	if fc-list -q "$IN"
	then # it's a font name
		FONT="$IN"
		IN="${2:-}"
		if [[ "$IN" =~ [0-9] ]]; then FONTSIZE="$IN"; IN="${3}"; fi
		[[ -z "$ALBET" ]] && [[ -n "$IN" ]] && ALBET="$IN"
	else # it's not a font; must be the alphabet
		[[ -z "$ALBET" ]] && ALBET="$IN"
	fi
fi
[[ -z "$ALBET" ]] && ALBET="$(alfabet)"

HTM1='<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"/><title></title><style type="text/css">pre { font-family: "'"${FONT}"'", sans-serif; font-size: '"${FONTSIZE}"'pt; background: transparent }</style></head><body><pre>'
HTM2='</pre></body></html>'

echo -e "${HTM1}\n${ALBET}\n${HTM2}" > "$TMP_HTM"
# Variables cannot be used directly here, because soffice needs a filename.
if soffice --convert-to pdf --outdir /tmp "$TMP_HTM"
then
	rm "$TMP_HTM"
	pdftocairo -pdf "${TMP_HTM%.html}.pdf" ./alfabet.pdf && \
		rm "${TMP_HTM%.html}.pdf"
	qpdf --qdf --object-streams=disable ./alfabet.pdf ./alfabet_editable.pdf
fi
