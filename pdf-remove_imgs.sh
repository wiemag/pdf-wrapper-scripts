#!/usr/bin/bash
version=0.02
declare -i VERBOSE=0 # Be talkative, and print messages extra info
declare -i optDep=0
if hash qpdf 2>/dev/null
then
	optDep=1
	Vopt='[-V] '
else
	Vopt=''
fi

# The script uses Ghostscript (gs)
#  -dFILTERIMAGE:  produces pdfs with raster images removed
#  -dFILTERTEXT:   produces pdfs with text elements removed
#  -dFILTERVECTOR: produces pdfs with vector drawings removed


OPT="${1:-}"
[[ -z "$OPT" || "$OPT" == '-h' || "$OPT" == '--help' ]] && {
	echo -e "\nRun:\n\t${0##*/} ${Vopt}input.pdf [input2.pdf [...]]\n";
	echo The script removes all images from the input PDF;
	echo -e "and creates \${input%.pdf}_noImgs.pdf\n";
	if ((optDep))
	then
		echo -e "-V  :  Be verbose and show which pages have images."
		echo -e "       Turned off if more than one PDF is processed.\n"
	fi
	echo -e "Dependency:        ghostscript (gs)"
	echo -e "Opt. dependency:   qpdf (printing extra info with -V option)\n"
	echo -e "${0##*/}, version ${version}\n"
	exit;
}

[[ "$OPT" == '-V' ]] && { ((optDep)) && VERBOSE=1; shift;}

(($#)) || { echo "Missing input PDF's."; exit 1;}
(($#>1)) && VERBOSE=0

for inPDF in "$@"
do
	[[ "$(file -b "$inPDF"|cut -d\  -f1)" == 'PDF' ]] || continue

	((VERBOSE)) && qpdf --show-pages --with-images "$inPDF"

	gs -dNOPAUSE -dQUIET -dBATCH \
   	   -dFILTERIMAGE \
   	   -sDEVICE=pdfwrite \
   	   -sOutputFile="${inPDF%.pdf}_noImgs.pdf" \
   	   "$inPDF"
done
