#!/bin/bash
version=1.00 # 2020-09-17 by wm
version=1.12 # 2024-07-08 by wm
inPlace=0	 # recompress PDF in place without backup
rm_meta=0    # remove metadata with qpdf's -empty parameter

function usage(){
cat <<USG

Script ${0##*/}, version ${version}
It uses qpdf to recopmress PDF's.

Run
      ${0##*/} [-i] [-r] input_PDFs | --help | -h

-i : Do not create .bak files (inplace recompression)
-r : Remove metadata with qpdf's -empty parameter

Recompression options are hard coded in the script.
USG
exit
}

[[ "$1" == '--help' ]] && usage

while getopts irhv OPT; do
    case "$OPT" in
        h|\?) usage;;
        v) echo ${0##*/}, version $version; exit;;
        o) OUTPUT=$OPTARG; OUTMODE=1;;
        i) inPlace=1;;
        r) rm_meta=1;;
    esac
done
shift $(expr $OPTIND - 1)
(($#)) || { echo Missing input file.; exit 1;}

for input in "$@"; do
  [[ $(file -b "$input"|cut -d\  -f1) == 'PDF' ]] && {
	tmpf=$(mktemp)
	permissions=$(stat -L -c "%a" "$input")
	# use --empty to remove meta data
	# escape spaces in input file names
	if qpdf "$( (($rm_meta)) && echo '--empty' || echo "${input// /\ }" )" \
		 --remove-unreferenced-resources=yes \
		 --flatten-annotations=all \
		 --compression-level=9 \
		 --object-streams=generate \
		 --recompress-flate \
		 --optimize-images \
		 --pages "$input" 1-z -- "${tmpf}"
	then
		(($inPlace)) || mv "$input" "${input}.bak"
		mv "$tmpf" "$input"
		chmod "$permissions" "${input}" # set permitions
	else
		# exit code 3 = warnings
		(($? == 3)) && mv "$tmpf" "${input%.pdf}_warnings.pdf" || rm "$tmpf"
	fi
  }
done
