#!/bin/bash
VERSION='0.3'
# By wm
# Stamp a PDF with a logo or a stamp file

# Check dependencies
DEPS="pdfinfo pdfjam pdftk awk bc"
for DEP in $DEPS; do
	[[ -x "$(command -v $DEP)" ]] || {
		echo Missing dependency:  command $DEP not available;
		exit 16;
	}
done
# qpdf is an optional dependency

usage_info() {
cat <<INFO

The script stamps a PDF-TO-BE-STAMPED with a STAMP-FILE.'
Usage:
   ${0##*/} -s STAMP-FILE [-m M] [-x X] [-y Y] PDF-TO-BE-STAMPED.pdf"
or
   ${0##*/} -r [-m M] -s STAMP-FILE  PDF-TO-BE-STAMPED.pdf"

-s STAMP-FILE  : must be a PDF/JPEG/PNG/TIF
-m M           : The stamp scaling factor; Default = 1
-x X and -y Y  : The values offset the stamp (-/+) left/right and (-/+) down/up
                 Both default to 0, which puts the stamp right at the middle.

-r             : Prints the maximum offsets (X, Y). Does not stamp the file.

-v             : Prints the script version.
-h             : Shows this usage information.
INFO
}

print_range() {
	echo -e "\nInput PDF size:  $X0 x $Y0"
	echo "Stamp size:      $X1 x $Y1"
	echo -e "Scaling factor:  $M\n"
	echo "|offset(x)| < $DX"
	echo "|offset(y)| < $DY"
}

STAMP=''       # path/to/the_stamp
RANGE=0        # print (1) or not (0) offsets
VERBOSE=0      # be verbose (1)
X=0; Y=0; M=1  # Location of the STAMP and its magnitude/scaling factor

# Parse dash-options
while getopts rs:x:y:m:?hv OPT; do
    case "$OPT" in
    	s) STAMP=$OPTARG;;
        h|\?) usage_info; exit;;
        v) VERBOSE=1;;
        x) X=$OPTARG;; # horizontal offset
        y) Y=$OPTARG;; # vertical offset
        m) M=$OPTARG;; # scaling magintude factor
        r) RANGE=1;;   # print max offsets for a given M and input files
    esac
done
shift $(expr $OPTIND - 1)


# Input file (the file to be stamped)
(($#)) || { usage_info; exit 1;}

# Check the input PDF
INPUT="$1"
[[ -f "$INPUT" ]] || { echo -e "\nWrong input PDF."; exit 2;}
[[ $(cut -d\  -f1 <(file -b "$INPUT"))  == PDF ]] || {
	printf '\n%s\n' "Input is not a PDF.";
	exit 3;
}
OUTPUT="${INPUT%.pdf}_stamped.pdf"


# Check the stamp file
if [[ -z "$STAMP" ]]
then
	echo -e "\nMissing stamp file."
	exit 4
else
	[[ -f "$STAMP" ]] || { echo -e "\nStamp file does not exist."; exit 5;}
fi

# Input PDF size X0 x Y0
Y0=$(pdfinfo "$INPUT" |awk '/Page size/ {print $3" "$5}')
X0=${Y0% *}
X0=$(LANG=C printf "%.0f" $X0) # might be necessary if X0 is not an integer
Y0=${Y0#* }
Y0=$(LANG=C printf "%.0f" $Y0) # might be necessary if Y0 is not an integer

# PDF rotation:  'Page rot'
ROT=$(pdfinfo "$INPUT"|awk '/Page rot/ {print $3}')
[[ $ROT -eq 90 || $ROT -eq 270 || $ROT -eq -90 ]] && {
	((VERBOSE)) && echo DEBUG Orientacja InputFile ROT=$ROT
	X1=$X0; X0=$Y0; Y0=$X1;    # Switch X0 with Y0
}
[[ $X0 -gt $Y0 ]] && ORIENT='--landscape' || ORIENT=''
TANG=$(bc <<< "scale=10; $Y0 / $X0") # Tangent of the input PDF


# Analyse STAMP; Stamp size X1 x Y1.
TYPE=$(file -b "$STAMP"|cut -d\  -f1)
case $TYPE in
	PDF) Y1=$(pdfinfo "$STAMP" |awk '/Page size/ {print $3" "$5}');;
	JPEG|PNG|TIFF) Y1=$(identify -format '%w %h' "$STAMP");;
esac
X1=${Y1% *}
Y1=${Y1#* }
# If M = 1, then STAMP is maximised to max X or max Y.
# The RATIO factor compensated for this maximalisation.
ratioX=$(bc <<< "scale=10; $X0 / $X1")
ratioY=$(bc <<< "scale=10; $Y0 / $Y1")
(( $(bc -l <<< "$ratioX < $ratioY") )) && RATIO=$ratioX || RATIO=$ratioY
# ±DX and ±DY are maximum offsets
DX="$(bc <<< "scale=10; ($X0 - $X1 * $M * $RATIO) / 2 / $TANG")";
DX="${DX%.*}"; [[ -z "$DX" ]] && DX=0
DY="$(bc <<< "scale=10; ($Y0 - $Y1 * $M * $RATIO) / 2 / $TANG")";
DY="${DY%.*}"; [[ -z "$DY" ]] && DY=0

((VERBOSE)) && {
	printf '\n%18s' "${INPUT}: "; echo  Page size $X0 x $Y0 $ORIENT
	printf '%18s' "${STAMP##*/}: "; echo  Page size $X1 x $Y1
	echo \|offset\(x\)\| \< $DX
	echo \|offset\(y\)\| \< $DY
	echo RATIO=$RATIO ratioX=$ratioX ratioY=$ratioY
	echo TANG=$TANG
}

((RANGE)) && {
	print_range
	exit;
}

((X > DX)) && echo Warning! The stamp too much to the right.
((Y > DY)) && echo Warning! The stamp is likely too high.
((X < -DX)) && echo Warning! The stamp too much to the left.
((Y < -DY)) && echo Warning! The stamp is likely too low.


# Create a PDF stamp file
# Useful parameters for left-upper corner logo on an invoice.
# pdfjam --paper 'a4paper' --landscape --scale 0.10 --offset '-11.70cm 8.4cm' --outfile /tmp/t4u-stamp.pdf "$STAMP"

STAMP_PDF="$(mktemp --suffix=.pdf)"
((VERBOSE)) && echo pdfjam $ORIENT --scale $M --offset \"$X $Y\" --outfile $STAMP_PDF \"$STAMP\"
pdfjam $ORIENT --scale $M --offset "$X $Y" --outfile "$STAMP_PDF" "$STAMP" 2>/dev/null || {
	echo "pdfjam did not work. Check the stderr of this command."
	exit 6;
}

# Stamp the input file
(($(stat --printf=%s $STAMP_PDF))) || { echo Error: STAMP_PDF is emty; exit 8;}
((VERBOSE)) && echo pdftk "$INPUT" stamp "$STAMP_PDF" output "$OUTPUT"
pdftk "$INPUT" stamp "$STAMP_PDF" output "$OUTPUT" &&
	((VERBOSE)) &&
		echo File \"$OUTPUT\" has been created.


# Cleaning
rm "$STAMP_PDF"
# Removing matadata
hash qpdf 2>/dev/null && {
	qpdf -empty -pages "$OUTPUT" 1-z -- "/tmp/${OUTPUT}x"
	mv "/tmp/${OUTPUT}x" "$OUTPUT"
} || {
	pdftk "$OUTPUT" dump_data_utf8| sed -n '1,/NumberOfPages:/'p| \
		awk '/Info/' | \
		sed 's/InfoValue: .*/InfoValue:/g' > "/tmp/${USER}-${OUTPUT}.txt"
	pdftk "$OUTPUT" update_info_utf8 "/tmp/${USER}-${OUTPUT}.txt"\
		output "/tmp/${OUTPUT}x"
	mv "/tmp/${OUTPUT}x" "$OUT"
	rm "/tmp/${USER}-${INPUT}.txt"
}
