#!/usr/bin/bash
version=0.05
# Default values
DEFOUTPUT="/tmp/${USER}_editable_alfabet.pdf" # the default output file
OUTPUT="$DEFOUTPUT"
DEFFONT='Arial'  # The default font
DEFSIZE='12'     # The default font size
debug=0          # Debugging mode switched off.

## Dependencies: coreutils, libreoffice-fresh(soffice),
#  poppler(pdftocairo), qpdf, fontconfig(fc-list)

## Considerations
# ? - replacing chars_in_string with grep -o .|sort -u|tr -d '\n' : No.
# ? - filter input string through iconv -t ASCII//TRANSLIT  : ź -> z : Maybe
# ? - not limiting the chars_in_string to x20-FF - if successful, unlimited possibilities : Maybe

## ToDo
# Modify the usage_msg: split into usage and help? : Undecided


usage_msg() {
echo -e "Run:\n\tstdin | ${0##*/} [options]"
echo -e "or\n\t${0##*/} [options] [input_string]\n"
cat <<INFO
 -f FONT                 ($DEFFONT)
 -s FONT_SIZE            ($DEFSIZE)
 -o OUTPUT_FILE          ($OUTPUT)
 -a             Just print the full 'alphabet' x20-FF and exit.
                --alphabet, and --alfabet work the same way

The sript creates a PDF page with a set of letters (including space) written
with the font_name of the font_size.

The set of letters is generated from the input strings (either from the pipe,
or from the regular positional argument). If no strings are input, a full set
of printable characters, codes x20-FF is generated for the output PDF.

INFO
echo -en "\e[1mfc-list : family -q 'font_name'\e[0m"
echo "  lists all available fonts/font families."
echo -en "\e[1mfc-list -q 'font_name'\e[0m"
echo "             answers whether the font is available."
echo -e "\e[1mLANG=C fc-list -f '%{family}, %{style}\\\n'|cut -d, -f1,2|sort -u\e[0m"
}

version_msg(){
	echo ${0##*/}, v.$version
}

alfabet_FF() {
local a=' !"#$%' # Alfabet. This line includes double quote.
	a+="&'()*+,-./0123456789:;<=>?@" # This line includes single quote.
	a+="$(printf '%s' {A..Z})[\]^_\`"
	a+="$(printf '%s' {a..z}){|}~"
	a+="¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿"  # Code 00AD usunięty (niedrukowalny?)
	a+="ÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"
	printf '%s' "$a"
}

chars_in_string() {
local inpStr="$(cat)"
local a="$(alfabet_FF)"
local -i i
local c
local outStr=''
	# unicodes below 00FF
	for ((i=0; i<${#a}; i++))
	do
		c="${a:i:1}"
		[[ ! "${outStr}" =~ "$c" ]] && [[ "$inpStr" =~ "$c" ]] && outStr+="$c"
	done
	printf '%s' "$outStr"
}

ALBET='' # holds a string (the full alfabet or a sub-set thereof)
FONT="$DEFFONT"
FONTSIZE="$DEFSIZE"

IN="${1:-}"
[[ "$IN" == '--help' || "$IN" == '-h' ]] && { usage_msg; exit;}
[[ "$IN" == '--version' || "$IN" == '-v' ]] && { version_msg; exit;}
[[ "$IN" == '-a' || "$IN" == '--alphabet' || "$IN" == '--alfabet' ]] && {
	# Folded, 60 characters per line
	echo -e "$(alfabet_FF)"|sed -e 's/.\{60\}/&\n/g';
	exit;
}

while getopts f:s:o:d OPT; do
    case "$OPT" in
	'f') FONT="$OPTARG";;
	's') FONTSIZE="$OPTARG";
	     [[ "$FONTSIZE" =~ [0-9].*[0-9]* ]] || {
			echo Incorrent font size.;
			exit 3;
	     };;
	'o') OUTPUT="$OPTARG";
	     [[ -e "$OUTPUT" ]] && echo Warning. File "$OUTPUT" exists. >&2;;
	'd') debug=1
	esac
done
shift $(($OPTIND - 1))

# Here, and not within the 'while getopts' loop, because
# the default FONT may not be installed.
if ! fc-list -q "$FONT"
then
	echo Font \"$FONT\" not available. >&2
	echo 'Run fc-list : family | sort [ | less ]' >&2
	echo 'or  fc-list : family | sort | grep '"$FONT" >&2
	exit
fi

## READY, STEADY, GO!

# The piped input has a priority over the positional argument.
if [[ -p /dev/stdin ]]
then
	ALBET="$(cat | chars_in_string | sed -e 's/.\{40\}/&\n/g')"
	((debug)) && echo -e "${ALBET}\nIncludes $(echo "$ALBET" |wc -c) chars" >&2
fi


[[ -z "$ALBET" ]] && ALBET="${1:-}"
[[ -z "$ALBET" ]] && ALBET="$(alfabet_FF | sed -e 's/.\{40\}/&\n/g')"

HTM1='<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"/><title></title><style type="text/css">pre { font-family: "'"${FONT}"'", sans-serif; font-size: '"${FONTSIZE}"'pt; background: transparent }</style></head><body><pre>'
HTM2='</pre></body></html>'

TMP_HTM="$(mktemp --suffix=.html)"  # .html
echo -e "${HTM1}\n${ALBET}\n${HTM2}" > "$TMP_HTM"
# Variables cannot be used directly here, because soffice needs a filename.
if soffice --convert-to pdf --outdir /tmp "$TMP_HTM" > /dev/null
then
	rm "$TMP_HTM"
	pdftocairo -pdf "${TMP_HTM%.html}.pdf" "${TMP_HTM%.html}_.pdf" && \
		rm "${TMP_HTM%.html}.pdf"
	qpdf --qdf --object-streams=disable "${TMP_HTM%.html}_.pdf" "$OUTPUT" && \
		rm "${TMP_HTM%.html}_.pdf" && echo "$OUTPUT"
fi
## Testing
# if not invoked from another script, this script need not echo the output filename
((debug)) && {
	echo "${BASH_SOURCE[0]}" >&2
	echo \${FUNCNAME[0]}=${FUNCNAME[0]} >&2
	[[ "${BASH_SOURCE[0]}" == "${0}" ]] && echo "$0 is run from terminal." >&2
}
