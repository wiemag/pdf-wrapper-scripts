#!/bin/bash
# Rotate and shift scanned image in PDF.
version=0.002 # Use on single page PDF's.

inpf="${1-}"
[[ -z "$inpf" ]] && { echo -e "\nMissing input file..."; exit 1;}
[[ -r "$inpf" ]] || { echo -e "\nInput not readable or non-existent."; exit 2;}
[[ "$(file -b "$inpf" | cut -d\  -f1)" == 'PDF' ]] || {
	echo -e "\nInput is not a PDF"; exit 3;
}

[[ "$inpf" == '-h' || "$inpf" == '--help' ]] && {
	echo ${0##*/} 'input.pdf top_corner_shift_up | [-h|--help]'
	echo -e "\nRun:\n   ${0##*/}\nVersion $version\n"
	echo Rotate and shift scanned image in PDF.
	echo This version works with single page PDF\'s.
	exit;
}

top_rh=${2-} 	# Supposed to be a number; if empty, enter graphical mode.
		# Graphical mode has not been worked on yet.
		# l/r keys rotate; left/right/up/down arrows shift
		# (still to be written -- a to-do)
# Until graphical mode is available, the missing $2 is treated like an error.
[[ -z "$top_rh" ]] && { echo -e "\nRotation size missing..."; exit 4;}

[[ $(pdfinfo "$inpf" |sed -n '/Pages:/s/^.* //p') -gt 1 ]] &&
echo WARNING. Modifies only the first page. The other are letf untouched.

[[ $(file -b "$inpf"|cut -d\  -f1) == 'PDF' ]] || {
	echo The input file is not a PDF.;
	exit 1;
}

tmpf=$(mktemp)
rm $tmpf
str8=${tmpf}_str8.pdf
tmpf="${tmpf}.pdf"
qpdf  --qdf --object-streams=disable "$inpf" "$tmpf"
echo DEBUG:  \$tmpf=$tmpf

stat --printf="DEBUG: inpf size: %s\n" "$inpf"
stat --printf="DEBUG: tmpf size: %s\n" "$tmpf"

delta_h=$(( -1 * $2)) 	# liczba (delta wysokości)
box0=($(grep -m1 ' cm$' <(strings "$tmpf")))

# Allow for page orientation
delta_w=$(echo "scale=10; -1 * $delta_h / ${box0[3]} * ${box0[0]}" |bc);
shift_x=$(echo "scale=10; $delta_w / 2" |bc)
shift_y=$(echo "scale=10; $delta_h / 2" |bc)
box1=(${box0[@]})
box1[1]=$delta_w
box1[2]=$delta_h
box1[4]=$shift_x
box1[5]=$shift_y

echo delta_h=$delta_h
echo delta_w=$delta_w
echo shift_x=$shift_x
echo ${box0[@]}
echo ${box1[@]}

sed -e 's/'"$(echo ${box0[@]})"'/'"$(echo ${box1[@]})"'/' "$tmpf" > "$str8"

stat --printf="DEBUG: str8 file size: %s\n" "$str8"
fix-qdf "$str8" > "${str8}.tmp"
mv "${str8}.tmp" "$str8"
stat --printf="DEBUG: str8 file size: %s\n" "$str8"
qpdf --remove-unreferenced-resources=yes \
     --flatten-annotations=all \
     --compression-level=9 \
     --object-streams=generate \
     --recompress-flate \
     --optimize-images \
     "$str8" "${inpf%.pdf}_str8.pdf"
stat --printf="DEBUG:  File size: %s\n" "${inpf%.pdf}_str8.pdf"

rm $str8 $tmpf
evince "${inpf}" &
evince "${inpf%.pdf}_str8.pdf" &
