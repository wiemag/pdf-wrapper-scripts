#!/bin/bash
# by wm 2023-07-28

function usagemsg(){
	echo -e "\n${0##*/} [-c][-s][-t tLANG][-a aLANG] | -h | --help\n"
	echo '-c = concatinate pages'
	echo '-s = spell check'
	echo 'tLANG = tesseract language (deu, eng, pol,...)'
	echo 'aLANG = aspell language (fr, en, ...)'
	echo '-h = --help = print a usage message'
}
function validPDF(){
	[[ "PDF" = $(file -b "$1"|sed 's/ .*//') ]];
}
function find_program {
    if ! hash $1 2>/dev/null; then
		echo "[!] $2 ('$1') not found" >&2
		exit -2
    fi
}
find_program "pdftk" "pdftk"
find_program "convert" "ImageMagick"
find_program "gs" "Ghostscript"
find_program "tesseract" "Tesseract"

tlang='pol' 	# pol, deu
alang='pl'  	# pl, de

[[ $1 == '--help' || $1 == '-h' ]] && { usagemsg; exit;}

while getopts ":sct:a:" opt; do
    case $opt in
	s)  spell=true; find_program "aspell" "Aspell";;
	c)  concat=1;;
	t)  tlang=$OPTARG;;
	a)	spell=true; alang=$OPTARG;;
	\?) echo "[!] invalid option: '-$OPTARG'"; exit -3;;
    esac
done
shift $((OPTIND-1))
(( $# )) || { echo "[!] Missing input file."; exit -1;}

fdir=$(mktemp -d)

for infile in "$@";
do
	set -e
	[[ "PDF" =~ $(file -b "$infile"|sed 's/ .*//') ]] && {

	pdftk "$infile" burst output "${fdir}/" # 2>/dev/null
	pushd "${fdir}" >/dev/null
	rm -f doc_data.txt

	echo "[*] splitting PDF"
	for i in pg_*.pdf; do
    	img="${i%.pdf}"
    	echo "[*] processing ${img/g_/age }"
    	gs -r300 -dINTERPOLATE -q -dNOPAUSE -sDEVICE=png16m -sOutputFile="${img}.png" ${i} -c quit
    	# normalising
    	magick "${img}.png" -modulate 120,0 "${img}.ok.png"
    	# ocr'ing
    	tesseract "${img}.ok.png" "${img}" -l "${tlang}" #Deutch
    	if [[ "${spell}" = true && -n "${alang}" ]]; then
        	aspell check "${img}.txt" -l "${alang:=fr}"
    	fi
	done
	echo "[*] cleaning"
	rm pg_*.png pg_*.pdf
	popd >/dev/null
	if ((concat)); then
    	cat "${fdir}"/pg_*.txt > "${infile%.pdf}.txt"
    else
    	ls -1 ${fdir}/*txt
    	for f in ${fdir}/*txt
    	do
    		mv "$f" "${infile%.pdf}_${f##*/pg_}"
    	done
	fi
	rm -r "${fdir}"
	} || {
	echo -e "\nSkipping $infile -- not a PDF";
	}
done
