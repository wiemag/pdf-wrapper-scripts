#!/bin/bash
# OCR multi-page PDF and output to std. output.
# Dependencies:  ghostscript, tesseract, tesseract-data-${tessLANG}
# No dependency checking.
# No parameter checking

tessLANG='pol'		# eng, ger, pol
input="$1"
tmpdir="$(mktemp -d)"

# extract images of the pages (note: resolution hard-coded)
# Engines: tiff24nc, tiffgray, tiffg4
gs -q -SDEVICE=tiffgray -r300x300 -sOutputFile="$tmpdir/page-%04d.tiff" -dNOPAUSE -dBATCH -- "$input"

# OCR each page and convert it into PDF
for page in "$tmpdir"/page-*.tiff
do
    base="${page%.tiff}"
    tesseract "${base}.tiff" "$base" -l ${tessLANG:=eng}
done

# print all the pages
cat "$tmpdir"/page-*.txt

# clean up
rm -rf -- "$tmpdir"
