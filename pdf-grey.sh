#!/bin/bash
VERSION=0.6

# DEPENDENCIES
# imagemagick(convert) replaced with imagemagick(magick)
# pdf2pdf_mono.sh
# pdf-recompress_with_qpdf.sh

# DEFAULT OPTIONS
method=6
ordered_dither=''	# see the od_ variables for the different methods
od_1='h4x4a'
od_2='o8x8'
od_5='h6x6a'
od_6='h6x6a'
posterize=4
inPlace=0
whthreshold='92%'
threshold='72%' # extra-monochroming if applicable for the method
density='288'
debug=0

function usage () {
 echo -e "\nUsage:\n\t\e[1m${0##*/} [-options] input[.pdf]\e[0m\n"
 echo -e "Produces a monochrome/dithered PDF:  \e[33;1minput_suffix.pdf\e[0
m\n"
 cat <<OPCJE
Options:

-m method_number          :  Defaults to $method
-l                        :  List methods and exit.
-o ordered_dither_method  :  For methods 1, 2, and 5 with different defaults
-p posterize              :  Defalts to $posterize (posterize=3 can produce good results)
-w whthreshold            :  White-threshold defaults to ${whthreshold}
-t threshold              :  Monochroming if applicable, (${threshold#* })
-d density                :  Defaults to ${density}.
-i                        :  Inplace conversion, defaults to 0 (No).
-h                        :  Print this usage message exit.
-H dither                 :  Prints available/suggested values of the parameter
-V                        :  Print version (${VERSION}).
OPCJE
echo -e "\n'\e[1m-monochrome\e[0m' option is hardcoded into the conversion command.\n"
}

function list_methods() {
echo all: density="$density", white-threshold="$whthreshold"
echo 1: Rec709Luminance, posterize+gradient:+clut, ordered-dither=${od_1}, mono, zip
echo 2: grayscale=Rec709Luminance, ordered-dither=${od_2}, monochrome, zip
echo 3: strip,interlace,sampling-factor,gaussian-blur,Gray,Lumi,zip
echo 4: normalize, remap pattern:gray50, monochrome, group4
echo 5: type grayscale, posterize 6+gradient:+clut, dither ${od_5}, mono, zip
echo 6: dither+posterize \'${od_5},4\', mono, group4
exit;
}

function param_values(){
[[ "${1,,}" == 'dither' ]] && {
cat <<DITH

  -ordered-dither h4x4a | h6x6a | h8x8a
  -ordered-dither h4x4o | h6x6o | h8x8o | h16x16o
  -ordered-dither o2x2  | o3x3  | o4x4  | o8x8

The same with posterization level, added to "-ordered-dither" after commas
The "6" is just an example
  -ordered-dither threshold,6
  -ordered-dither checks,6
  -ordered-dither o2x2,6
  -ordered-dither o4x4,6
  -ordered-dither o8x8,6
DITH
} || usage
}

method_basic_info() {
	echo \("$1"\) Do the magic with "$2"
}
method_1(){
local me="${FUNCNAME[0]##*_}"
local dith="${1:-$od_1}"
  method_basic_info "$me" "lumi and gradient posterized $post"
  output+="${me}_lumi_grad_post${posterize}.pdf"
  ((debug)) && {
  	echo -e "DEBUG:\n\t-density $density\n\t-white-threshold $whthreshold"
  	echo -e "\t-grayscale Rec709Luminance\n\t-size 2x300"
  	echo -e "\t-posterize $posterize\n\t-negate gradient:"
  	echo -e "\t-clut\n\t-ordered-dither $dith"
  	echo -e "\t-background white\n\t-gravity center"
  	echo -e "\t-monochrome\n\t-compress zip"
  }
  magick -density "$density" "$input" -white-threshold "$whthreshold" \
	-grayscale Rec709Luminance \
	-size 2x300 -posterize "$posterize" -negate gradient: -clut \
	-ordered-dither "$dith" \
	-background white -gravity center \
	-monochrome \
	-compress ZIP \
	"$output" &&
  pdf2pdf_mono.sh -i -q -t"${threshold}" "${output}" &&
  pdf-recompress_with_qpdf.sh -ir "${output}"
}

method_2(){
local me="${FUNCNAME[0]##*_}"
local dith="${1:-$od_2}"
  method_basic_info "$me" "Rec709Luminance"
  output+="${me}_lumi.pdf"
  ((debug)) && {
  	echo -e "DEBUG\n\tTwo-step process."
  	echo -e "Step 1\n\t-density $density\n\t-white-threshold $whthreshold"
  	echo -e "\t-grayscale Rec709Luminance\n\t-compress JPEG"
  	echo -e "Step 2\n\t-ordered-dither $dith"
  	echo -e "\t-monochrome\n\t-comress ZIP"
  }
  # No need for -white-threshold "$whthreshold" \
  magick -density "$density" "$input" \
	-grayscale Rec709Luminance \
	-compress JPEG \
	"$output" 	&& # JPEG lepsze niż ZIP, lepsze niż nic
  pdf-recompress_with_qpdf.sh -ir "$output" &&
  magick -density "$density" "$output" \
	-ordered-dither "$dith" \
	-monochrome \
	-compress ZIP \
	"${output%.pdf}m.pdf" &&
	pdf-recompress_with_qpdf.sh -ir "${output%.pdf}m.pdf"
  # ZIP lepsze niż JPEG, lepsze niż Group4
}

method_3(){
local me="${FUNCNAME[0]##*_}"
  method_basic_info "$me" "strip, interlace, sampling_factor, gauss-blur"
  output+="${me}.pdf"
  # No need for -white-threshold "$whthreshold"
  ((debug)) && {
  	echo -e "DEBUG\n\tTwo-step process."
  	echo -e "Step 1\n\t-density $density\n\t-white-threshold NOT-USED"
  	echo -e "\t-interlace JPEG\n\t-sampling-factor 4:2:0"
  	echo -e "\t-gaussian-blur 0.05\n\t-colorspace Gray -quality 20"
  	echo -e "\t-grayscale Rec709Luminance\n\t-compress JPEG"
  	echo -e "Step 2\n\t-fill white\n\t-fuzz 75%\n\t+opaque '#000000'"
  	echo -e "\t-comress ZIP"
  }
  magick -density "$density" "$input" \
  	-strip -interlace JPEG -sampling-factor 4:2:0 -gaussian-blur 0.05 \
  	-colorspace Gray -quality 20 \
  	-grayscale Rec709Luminance \
  	-compress JPEG  \
  	"$output" &&
  pdf-recompress_with_qpdf.sh -ir "$output" &&
  #the extra arguments cleans shadows from previous pages
  magick -density "$density" "$input" \
	-strip -interlace JPEG -sampling-factor 4:2:0 -gaussian-blur 0.05 \
	-colorspace Gray -quality 20 \
	-fill white -fuzz 75% +opaque "#000000" \
	-compress JPEG \
	"${output%.pdf}_2.pdf"
  pdf-recompress_with_qpdf.sh -ir "${output%.pdf}_2.pdf"
}

method_4(){
local me="${FUNCNAME[0]##*_}"
  method_basic_info "$me" "remap pattern:gray50 and monochromed group4"
  output+="${me}_grey${gray}_mono.pdf"
  ((debug)) && {
  	echo -e "DEBUG:"
  	echo -e "\t-density $density\n\t-white-threshold $whthreshold"
  	echo -e "\t-normalize\n\t-remap pattern:gray50"
  	echo -e "\t-monochrome\n\t-comress group4"
  }
  magick -density "$density" "$input" -white-threshold "$whthreshold" \
	-normalize -remap pattern:gray50 \
	-monochrome \
	-compress group4 \
	"$output"
  pdf-recompress_with_qpdf.sh -ir "${output}"
}

method_5(){
local me="${FUNCNAME[0]##*_}"
local dith="${1:-$od_5}"
  method_basic_info "$me" "gradient posterized 6"
  output+="${me}_grad_post6.pdf"
  ((debug)) && {
  	echo -e "DEBUG\n\tTwo-step process."
  	echo -e "Step 1\n\t-density $density\n\t-white-threshold $whthreshold"
  	echo -e "\t-grayscale"
	echo -e "\t-size 10x240 -posterize 6 -negate gradient:"
	echo -e "\t-clut"
	echo -e "\t-background white -gravity center"
  	echo -e "\t-compress zip"
  	echo -e "Step 2\n\t-density 288\n\t-ordered-dither ${dith}"
  	echo -e "\t-monochrome\n\t-comress zip"
  }
  magick -density "$density" "$input" -white-threshold "$whthreshold" \
	-type grayscale \
	-size 10x240 -posterize 6 -negate gradient: \
	-clut \
	-background white -gravity center \
	-compress zip \
	"$output" &&
  pdf-recompress_with_qpdf.sh -ir "$output" &&
  magick -density 288 "$output" \
	-ordered-dither "$dith" \
	-monochrome \
	-compress zip \
	"${output%.pdf}m.pdf" &&
  pdf-recompress_with_qpdf.sh -ir "${output%.pdf}m.pdf"
}

method_6(){
local me="${FUNCNAME[0]##*_}"
local dith="${1:-$od_6},${posterize}"
  method_basic_info "$me" "dither,posterized combo $dith"
  output+="${me}_dith-post_${dith}.pdf"
  ((debug)) && {
  	echo -e "DEBUG"
  	echo -e "\t-density $density\n\t-white-threshold $whthreshold"
  	echo -e "\t-ordered-dither ${dith}"
  	echo -e "\t-monochrome\n\t-comress Group4   # Much better than zip here"
  }
  magick -density "$density" "$input" -white-threshold "$whthreshold" \
	-ordered-dither "$dith" \
	-monochrome \
	-compress Group4  \
	"${output}" &&
  pdf-recompress_with_qpdf.sh -ir "${output}"
}

[[ "${1-}" == '--help' ]] && usage && exit

while getopts "m:lo:p:w:t:d:ihH:VD" flag
do
 case "$flag" in
  m) method="$OPTARG";;
  l) list_methods;;
  o) ordered_dither="$OPTARG";;
  p) posterize="$OPTARG";;
  d) density="$OPTARG";;
  w) whthreshold="$OPTARG";;
  t) threshold="$OPTARG";;
  i) inPlace=1;;
  h) usage; exit;;
  H) param_values "$OPTARG";;
  V) echo -e "${0##*/} version ${VERSION}" && exit;;
  D) debug=1;;
 esac
done
# Remove the options parsed above.
shift `expr $OPTIND - 1`
(( $# )) || { echo Missing input file.; exit 1;}

tmpf=$(mktemp --suffix=.pdf) # temporary file

for input in "$@"
do
  output="${input%.pdf}_m."
  case "$method" in
	1) method_1 "$ordered_dither";;
	2) method_2 "$ordered_dither";;
	3) method_3;;
	4) method_4;;
	5) method_5 "$ordered_dither";;
	6) method_6 "$ordered_dither";;
	*) echo Wrong method.; exit 1;;
  esac
done

chmod --reference="$input" "$output"
((inPlace)) && "$output" "$input"

exit 0 # Koniec

# INNE METODY DO SPRAWDZENIA

echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -grayscale average "${input%.pdf}_average_test_${method}.pdf"
# Seems to generate large files.

echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -colorspace OHTA -channel r -separate +channel "${input%.pdf}_OHTA_test_${method}.pdf"
# Seems to generate large files.

echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -intensity average -colorspace gray "${input%.pdf}_average_test_${method}.pdf"
# Seems to generate large files.

echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -colorspace HSI -channel blue -separate +channel "${input%.pdf}_HSI_test_${method}.pdf"
# Seems to generate large files.

echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -fx '(r+g+b)/3' -colorspace Gray "${input%.pdf}_gray_fx_average_test_${method}.pdf"


echo \(${method}\) Do the magic with ...
magick -density 288 "$input" -colorspace Gray -channel All -random-threshold 0x100% -compress zip xx_Gray,_channel_ALL_random_thresh_0x100%.pdf
echo Popatrz na ostatnią linię skryptu
magick -density 288 "$input" -colorspace Gray -ordered-dither o8x8 -compress zip xx_o8.pdf
