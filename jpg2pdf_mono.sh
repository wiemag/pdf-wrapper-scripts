#!/bin/bash
# The script converts JPEG's, PNG's and other convertable file
# into monochrome PDF's
source bashf.sh || {
	echo Install bashf or put bashf.sh in the \$PATH.
	exit 1
}
show_version() { echo "${0##*/} 2.06 (2020-05-21)"; exit; }

compress="Group4"
threshold=""
ARG_PARSE_OPTS+=(
	t threshold --desc='The threshold [0..100%] whitens/blackens pixels' =:val
	c compress --desc='Compression type as defined in "magick -list compress"' =:val
	v version --desc='Print script version no and exit' '{ show_version; }'
)
ARG_PARSE_REST=(--opt-var=files --opt-min=0)
arg_parse "$@"
has_val files || die_return 2 "No input files" # or opt-min=1

convert_args=()
[ -z "$compress" ] || convert_args+=(-compress "$compress")
[ -z "$threshold" ] || convert_args+=(-threshold "$threshold")
convert_args+=(-enhance -monochrome)

for f in "${files[@]}"
do
	[ -f "$f" ] || die_return 3 "$f does not exist"
	magick "$f" ${convert_args[@]} "${f%.*}.pdf"
done
