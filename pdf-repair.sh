#!/bin/bash
# by wm (dif)
# Repair a corrupted PDF
# Some tips:  http://milan.kupcevic.net/ghostscript-ps-pdf/
#
VERSION=0.6
METHOD=3 	# default method
USAGE='\nThe script repairs corrupted PDFs.\n'
USAGE+="\nUsage:\n\t${0##*/} [-m METHOD] [-i] INPUT.pdf | -h | --help\n"
USAGE+='\n-i : an in-place repair (modifies the input file)\n'
USAGE+="\nMethods  (default = "$METHOD")"
USAGE+='\n1  : pdftops    --> ps2pdf     --> INPUT_repaired.pdf'
USAGE+='\n2  : pdftops    --> gs (print) --> INPUT_repaired.pdf'
USAGE+='\n3  : pdftocairo --> gs (print) --> INPUT_repaired.pdf\n'
USAGE+='\n30 : pdftocairo --> gs (print/screen 72 dpi)'
USAGE+='\n31 : pdftocairo --> gs (print/eboot 150 dpi)'
USAGE+='\n32 : pdftocairo --> gs (print/printer 300 dpi) (like option 3)'
USAGE+='\n33 : pdftocairo --> gs (print/prepress 300 dpi, colour preserving)'
USAGE+='\n34 : pdftocairo --> gs (print/default) (like prepress)'
[[ $1 = '--help' ]] && { echo -e $USAGE; exit;}
INPLACE=0
TMPF="$(mktemp)"
TMPOUT="$(mktemp)"

# Parse dash-options
while getopts m:?hvi OPT; do
    case "$OPT" in
        h|\?) echo -e "$USAGE"; exit;;
        v) echo ${0##*/}, version "$VERSION"; exit;;
        m) METHOD="$OPTARG";;
        i) INPLACE=1;;
    esac
done
shift $(($OPTIND - 1))

(($#)) || { echo -e "\nNo files to repair.\nUse --help or -h."; exit;}

for INPUT in "$@"; do
	if [[ $(file -b --mime-type "$INPUT") == 'application/pdf' ]]; then
		case $METHOD in
		'1') pdftops "$INPUT" "$TMPF"; ps2pdf "$TMPF" "$TMPOUT";;

		'2') pdftops "$INPUT" "$TMPF";
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/default \
				"$TMPF" >/dev/null;;

		'3'|'32') pdftocairo -pdf "$INPUT" "$TMPF";
		# gs command not neccesary here, but may reduce the size considerably.
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/printer \
				"$TMPF" >/dev/null;;
		'30') pdftocairo -pdf "$INPUT" "$TMPF";
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/screen \
				"$TMPF" >/dev/null;;
		'31') pdftocairo -pdf "$INPUT" "$TMPF";
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook \
				"$TMPF" >/dev/null;;
		'33') pdftocairo -pdf "$INPUT" "$TMPF";
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress \
				"$TMPF" >/dev/null;;
		'34') pdftocairo -pdf "$INPUT" "$TMPF";
			gs -o "$TMPOUT" -sDEVICE=pdfwrite -dPDFSETTINGS=/default \
				"$TMPF" >/dev/null;;
		esac
		# remove mata data if qpdf is installed
		hash qpdf 2>/dev/null && {
			qpdf --empty --pages "$TMPOUT" -- "$TMPF";
			mv "$TMPF" "$TMPOUT";
		}
		# retain or set file permitions as those of the input file
		chmod --reference="$INPUT" "$TMPOUT"
		((INPLACE)) && mv "$TMPOUT" "${INPUT}" || \
			mv "$TMPOUT" "${INPUT%.pdf}_repaired_${METHOD}.pdf"
		(($?)) || echo "[OK] '${INPUT}'";
	else
		[[ -f "$INPUT" ]] && echo "[!] '$INPUT' is not a PDF." ||
			echo "[!] '$INPUT' does not exist."
	fi
done
[[ -e "$TMPF" ]] && rm "$TMPF" || :
