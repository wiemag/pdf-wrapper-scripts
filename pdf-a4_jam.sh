#!/bin/bash
# Convert a PDF file into an A4 format.
# by Wiesław Magusiak, 2024-04-09
VERSION=0.01
function usemsg (){
	echo -e "\nUsage:\n\tpdf-a4.sh [-l] [--nocheck] file.pdf"
	echo -e "\nConverts input file to A4, creating \${file}_a4.pdf"
	echo -e "\n\e[1mThis is a pdfjam (texlive-binextra) wrapper.\e[0m"
}

function depcheck (){
	hash $1 2>/dev/null || {
	echo -e "\nMissing dependency.\nCommand \e[1m${1}\e[0m needed."
	[[ -n $2 ]] && echo -e "Install the \e[1m${2}\e[0m package."
	return 1
	}
}

depcheck pdfinfo poppler || exit
depcheck awk gawk || exit
depcheck pdfjam texlive-binextra || exit

ArgNo="$#"
FILE="${1:-}"
((ArgNo==0)) || [[ "$FILE" = '--help' || "$FILE" = '-h' ]] && { usemsg; exit;}
NCK=0 		# Check the file before converting?
LND=''      # vertical / not horizontal (not landscape)

while ((ArgNo--))
do
	[[ "$FILE" == '--nocheck' ]] && NCK=1
	[[ "$FILE" == '-l' || "$FILE" == '--landscape' ]] && LND="--landscape"
	((ArgNo)) && { shift; FILE="${1:-}";}
done
[[ -f "$FILE" ]] || { echo File $FILE does not exist; exit 1;}

if ! ((NCK)); then
	depcheck qpdf qpdf && {
		qpdf --check "$FILE" 1>/dev/null
		[[ $? -eq 0 ]] || echo '----------------------'
	}
	x=$(pdfinfo "$FILE" |awk '/Page size/ {print $3" "$5}')
	y=${x#* }; x=${x% *}
	a4x=595; a4y=842
	scale=$(echo $x $a4x | awk '{print $1/$2}')
	[[ $scale = 1 ]] && {
		echo -e "\nFile $FILE is already A4."
		exit
	}
fi

OUTPUT=${FILE%.pdf}
OUTPUT=${OUTPUT%.PDF}_a4.pdf
# The $LND variable cannot be encircled with double-quote marks
pdfjam -q --outfile "$OUTPUT" $LND --paper a4paper "$FILE"
