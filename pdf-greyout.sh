#!/bin/bash
version=0.2 # May 2024
density=288
usage(){
	echo -e "\n\t${0##*/} [-d density] input.pdf\n\tversion ${version}\n"
	echo Convert a PDF to grayscale, using the \'monochrome\' ImageMagick flag.
	echo Parameter \'density\' defaults to 288.
	echo "'white-threshold' set to 96%. 'threshold' is not used at all."
	echo "The 'Group4' compression-method is used."
	exit $1;
}
(($#)) && input="$1" || usage 1

if [[ "${input:0:2}" == '-d' ]] # in case the value is glued to the '-d'
then
	density="${input:2}" # Try to read the density value, and if unsuccessful,
	[[ -z "$density" ]] && { shift; density="$1";} # read from the next argument
	((density)) || { echo Wrong density.; exit 2;} # Not a number!
	shift
fi
(($#)) && input="$1" || usage 3

magick -density 288 "$input" \
	-white-threshold 96% \
	-monochrome \
	-compress Group4 \
	"${input%.pdf}_gray.pdf" && \
		chmod --reference="$input" "${input%.pdf}_gray.pdf"
