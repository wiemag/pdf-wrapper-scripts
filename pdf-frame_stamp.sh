#!/bin/bash
#
# The script puts a white or coloured frame along the edges
# of a pdf page, masking anything that is there.
# Run:
#   pdf-frame-stamp.sh [-c 'colors'] input.pdf 'margins'
# Warning. It's working only for portrait a4 PDF's rotated 0,90,180,or 270 degs.

VERSION=1.04 	# 2020-09-17
inplace=0    	# 0 = create _mod.pdf file, 1 = do not back up

function usage(){
cat << HELP

${0##*/} [-i] [-c 'colors'] input.pdf 'margins' | [-v]

Where
  -v        : prints the script version and exits
  -i        : inplace / no backup processing
  -c        : defines the 'r g b' colors
  -h/--help : prints a help/usage  message
  margins   : 'left [top [right [bottom]]]'

Examples
  colors               : '0 0 0' =black, '1 1 1' =white
  margins="10"         : all four margins are 10 points wide
  margins="10 12.5mm"  : left and right margins are 10 pts wide
                       : top and bottom are 12.5mm wide
  margins="10 4mm 12"  : bottom margin is 4mm wide (as top one)
  margins="10 0 2 4mm" : all margins defined individually
HELP
}

# The double quotes below is a trick to make sure there's a string
# on the left, as command 'type' does not output anythinag
# in case of an error.
# If 1st arg is a function, run it. If not, print it.
function abortf(){
	[ $(type -t "$1")"" == 'function' ] && "$1" || echo -e "$1"
	exit "${2:-0}"
}

function chk_color(){
 local col=($1) i msg="Wrong color definition.\n"
 (( ${#col[@]} == 3 )) || {
  msg+="Use three numbers [0..1] inside single or doube quotes."
  abortf "$msg" $2;
 }
 for ((i=0; i<3; i++));
 do
  [[ ${col[$i]} =~ ^[0-9]*([.][0-9]+)?$ ]] ||
	  abortf "${msg}${col[$i]} is nor a number." $2
 done
}

frm_color='1 1 1' 	# frame colour

(($#)) || abortf "No input PDF.\nRun with -h/--help for a usage message." 1


while getopts "hvic:" flag
do
    case "$flag" in
	c) frm_color="$OPTARG";
       chk_color "$frm_color" 5;;
	i) inplace=1;;
	h) abortf usage;;
	v) abortf "${0##*/} version ${VERSION}";;
    esac
done
# Remove the options parsed above.
shift `expr $OPTIND - 1`

input="$1"
shift

[[ -f "$input" ]] || { abortf  "File '$input' does not exist." 2;}
[[ $(file -b "$input" | cut -d\  -f1) == 'PDF' ]] ||
	abortf "File '$input' is not a PDF." 3
(($#)) || abortf "No margins defined." 4

# Dependencies: poppler (pdfinfo), gawk (awk), bc (bc -calculator)
#               qpdf or pdftk (pdf manipulation programs)
#
# An International Inch is exactly .0254 m (25.4 mm) since the International Yard & Pound agreement in 1959.
# The older Imperial Inch (pre-1959 value) used in the United Kingdom from 1845–1959 was measured to be exactly 25.399956 mm
# The U.S. Survey Inch is still commonly used by surveyors for Land Surveying and is defined to be 1/12 of a U.S. Survey Foot or exactly 25.4000508001 mm (1.000002″).

function compute(){
	echo "scale=15; $1"|bc
}

function parse_margins(){
	local s margs units i m=0 N=0
	s="$1"
	s="${s//,/.}" 	# Precaution: commas into decimal points
	margs=($s)
	units=($unitW $unitH)
	N=${#margs[@]}
	for ((i=0; i<$N; i++)); do
		m=${margs[$i]%% *}
		if 	[[ ${m: -2} == 'mm' ]]
		then
			m=$(compute "${m%mm} * ${units[$((i % 2))]}")
		fi
		margins+=($m)
	done
	case $N in
		1) margins+=(${margins[@]}); margins+=(${margins[@]});;
		2) margins+=(${margins[@]});;
		3) margins+=(${margins[1]});;
	esac
}

function rotate_margins(){
local i k
  #echo DEBUG: margins = ${margins[@]}
  case $rot in
	90)  i=${margins[0]};
	     for ((k=0;k<3;k++))
	     do margins[$k]=${margins[$((k+1))]}; done;
 	     margins[3]=$i;;
	180) i=${margins[0]}; margins[0]=${margins[2]}; margins[2]=$i;
	     i=${margins[1]}; margins[1]=${margins[3]}; margins[3]=$i;;
	270) i=${margins[3]};
	     for ((k=3;k>0;k--))
	     do margins[$k]=${margins[$((k-1))]}; done;
 	     margins[0]=$i;;
  esac
  #echo DEBUG: margins = ${margins[@]}
}

function mkframe(){
ps2pdf - - <<PS_STREAM
%!PS
%
% Set the page size to A4/A3/...
<< /PageSize [$width $height] >> setpagedevice
%

2 setlinewidth
${frm_color}  setrgbcolor       % Set the colour 'R G B'
newpath

% BOTTOM
0 0 moveto 	% move poiter to left-lower corner
${width} 0 lineto
0 ${margins[3]} rlineto
-${width} 0 rlineto
closepath 	% close the shape/rectangle
fill 		% and fill it with the defined colour

% LEFT
0 0 moveto 	% move poiter to left-lower corner
${margins[0]} 0 lineto
0 ${height} rlineto
-${margins[0]} 0 rlineto
closepath 	% close the shape/rectangle
fill 		% and fill it with the defined colour

% RIGHT
$(compute "$width - ${margins[2]}") 0 moveto 	% move pointer to left-lower corner
${margins[2]} 0 rlineto
0 ${height} rlineto
-${margins[2]} 0 rlineto
closepath 	% close the shape/rectangle
fill 		% and fill it with the defined colour

% TOP
0 $(compute "$height - ${margins[1]}") moveto 	% move poiter to left-lower corner
${width} 0 rlineto
${width} ${height} lineto
0 ${height} lineto
closepath 	% close the shape/rectangle
fill 		% and fill it with the defined colour

showpage
PS_STREAM
}

####:::...BEGIN...:::#########################################

point_mm=$(compute "25.4 / 72")

# general width of a point, 1/72 of an inch
width="$(pdfinfo "$input" | awk '/Page size/ {print $3" "$5}')"
height=${width#* }
width=${width% *}
unitH=$(compute "1 / $point_mm") # 210x297mm (a4)
unitW=$(compute "1 / $point_mm") # 210x297mm (a4)
rot=$(pdfinfo "$input" | awk '/Page rot/ {print $3}')

declare -a margins

tmp_frame=$(mktemp)
tmp_output=$(mktemp)

parse_margins "$1"
#echo DEBUG: ${margins[@]}
rotate_margins

mkframe > $tmp_frame
# Rotate the frame if necessary
(($rot)) && qpdf --replace-input --rotate=$rot $tmp_frame

(($inplace)) || cp "$input" "${input}.bak"
# if inastalled, use qpdf to do the job. If not, try to use pdftk.
if hash qpdf 2>/dev/null
then
  qpdf "$input" --overlay $tmp_frame --from= -repeat=1 -- $tmp_output && mv $tmp_output "$input"
else
  if hash pdftk 2>/dev/null
  then
	pdftk "$input" stamp $tmp_frame output $tmp_output && mv $tmp_output "$input"
  else
	echo You need to install qpdf or pdftk.
  fi
fi
rm $tmp_frame
