#!/bin/bash
# Resize/Scale the input PDF in such a way that the longer edge is
# scaled to match the a4 long edge. The short edge is scaled with
# the same ratio/scaling-factor.
version=0.01

# dependencies:
# command bc, package bc - calculator
# command pdfjam, package texlive-binextra
#
usage_info() {
echo -e "\nRun:\n\t${0##*/} input.pdf\n"
echo Resize the input PDF in such a way that the longer edge is scaled
echo -e "to match the \e[1ma4\e[0m long edge. The short edge is scaled with"
echo the same ratio/scaling-factor.
echo -e "\n\e[1mIt is a pdfjam wrapper.\e[0m"
}

version_info() {
echo -e "\n${0##*/}, version ${version}"
}

(( $# )) || { echo Missing input PDF; exit 1;}
inPDF="${1:-}"
[[ "$inPDF" == '-h' || "$inPDF" == '--help' ]] && { usage_info; exit;}
[[ "$inPDF" == '-v' || "$inPDF" == '--version' ]] && { version_info; exit;}

x=0; y=0 	# pdf size

longer-edge_ratio_to_a4() {
local x="$1"
local y="$2"
local yy
local a4y='841.89' 	# a4x='595.276'
	(( $(bc <<<"${x} < ${y}") )) && yy="$y" || yy="$x"
	echo $(bc <<<"scale=5; ${a4y}/${yy}")
}

scale_it() {
	LC_NUMERIC=C printf "%.2f" $(bc <<<"scale=5; ${1}*${2}") # Round to integer
	#echo $(bc <<<"scale=3; ${1}*${2}") # Round to 3 decimals
}

read x y < <(pdfinfo "$1"|awk '/Page size:/ {print $3" "$5}')
ratio="$(longer-edge_ratio_to_a4 "$x" "$y")"
x1="$(scale_it "$x" "$ratio")"
y1="$(scale_it "$y" "$ratio")"

#pdfjam --suffix converted --papersize '{'"$x"'px,'"$y"'px}' --scale "$ratio" --trim "-6cm -1cm 13cm 8cm" "$inPDF"
pdfjam -o /tmp --suffix resized --papersize '{'"$x1"'px,'"$y1"'px}' "$inPDF" &&
	mv -b -S .bak /tmp/${inPDF%.pdf}-resized.pdf "${inPDF}"
