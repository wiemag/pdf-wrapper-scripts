#!/bin/bash
version='2.00' # input from pipe added
usg_msg() {
	echo -e "\nUsage:\n\t${0##*/} INPUT.pdf [OUTPUT.pdf]\nor"
	echo -e "\tstdin | ${0##*/}\n"
	echo The script creates an editable version of the input file.
	echo -e "If OUTPUT.pdf is not envoked, INPUT_editable.pdf is created."
	echo -e "\nThe output goes to stdout if input comes from stdin."
}
ver_msg() {
	echo -e "\n${0##*/}, version $version"
}

if [ -p /dev/stdin ]
then
	IN="$(mktemp)"
	cat > "$IN"
	qpdf --qdf --object-streams=disable "$IN" - && rm "$IN"
else
	IN="${1:-}"
	[ "$IN" = '-v' -o "$IN" = '--version' ] && { ver_msg; exit 1;}
	[ -z "$IN" -o "$IN" = '--help' ] && { usg_msg; exit 1;}
	OUT="${2:-}"
	[[ -z "$OUT" ]] && OUT="${IN%.pdf}_editable.pdf"
	permissions=$(stat -L -c "%a" "$IN")
	qpdf --qdf --object-streams=disable "$IN" "$OUT"
	chmod "$permissions" "$OUT" # transfer permissions
fi
